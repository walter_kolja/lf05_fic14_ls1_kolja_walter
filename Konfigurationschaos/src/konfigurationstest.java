
public class konfigurationstest
{public static void main(String[] args) {
	// Uebung 1 Variablen
	int cent = 70;
	double maximum = 95.50;
	System.out.println("Uebung 1 Variablen:");
	System.out.println("Cent: " + cent);
	cent = 80;
	System.out.println("Cent: " + cent + " , maximum: " + maximum);
	
	// Uebung 2 Variablen
	boolean test1 = true;
	short test2 = -1000;
	float test3 = 1.255f;
	char test4 = '#';
	System.out.println("Uebung 2 Variablen: " + test1 + " | " + test2 + " | " + test3 + " | " + test4);
	
	// Uebung 3 Variablen
	String test5 = "Testuung erfolgreich?";
	final short check_nr = 8765;
	System.out.println("Uebung 3 Variablen: " + test5 + " UND " + check_nr);
	
	// Uebung 1 Operatoren
	System.out.println("Uebung 1 Operatoren:");
	int ergebnis = 4 + ( 8 * 9 ) - 1;
	System.out.println("4 + 8 * 9 - 1 = " + ergebnis);
	byte zaehler = 1;
	zaehler++;
	System.out.println("1 + 1 = " + zaehler);
	System.out.println("22 : 6 = " + 22/6);
	
	// Uebung 2 Operatoren
	System.out.println("Uebung 2 Operatoren:");
	byte schalter = 10;
	System.out.println("Schalter kleiner als 12 aber gr��er als 7: " + (7 < schalter && schalter < 12));
	System.out.println("Schalter ist ungleich 10 ODER gleich 12: " + (schalter != 10 || schalter == 12));

	


