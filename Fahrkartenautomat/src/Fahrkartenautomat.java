﻿import java.text.DecimalFormat;
import java.util.Scanner;
import utils.Bestellung;

class Fahrkartenautomat {
	private static Bestellung bestellung;

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		while(true) {
			bestellungErfassen(tastatur);
		
			
			rueckgeldAusgeben(tastatur);
			fahrscheineAusgeben();
			//tastatur.close();
		}
	}

	private static void bestellungErfassen(Scanner tastatur) {
		double zuZahlenderBetrag = 0;
		
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		System.out.println("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
		System.out.println("  Tageskarte Regeltarif AB [8,60 EUR] (2)");
		System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		System.out.println("");
		
		boolean eingabeKorrekt = false;
		int auswahl = 0;
		while(!eingabeKorrekt) {
			System.out.print("Ihre Auswahl: ");
			auswahl = tastatur.nextInt();
			if(auswahl > 0 && auswahl < 4)
				eingabeKorrekt = true;
			else
				System.out.println(">>falsche Eingabe<<");
		}
		
		switch(auswahl) {
			case 1:
				zuZahlenderBetrag = 2.9;
				break;
			case 2:
				zuZahlenderBetrag = 8.6;
				break;
			case 3:
				zuZahlenderBetrag = 23.5;
				break;
		}
		
		System.out.print("Anzahl der Tickets (1-10): ");
		byte anzahlFahrscheine = tastatur.nextByte();
		
		if(anzahlFahrscheine > 10 || anzahlFahrscheine < 1) {
			System.out.println("FEHLER! Es dürfen nur 1-10 Fahrscheine gleichzeitig gekauft werden.\n"
					+ "Anzahl der Tickets wird zurückgesetzt auf 1.");
			anzahlFahrscheine = 1;
		}

		bestellung = new Bestellung(anzahlFahrscheine, zuZahlenderBetrag);
	}

	private static double eingezahlterGesamtbetrag(Scanner tastatur) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;

		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < bestellung.getGesamtBetrag()) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", (bestellung.getGesamtBetrag() - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag;
	}

	private static void fahrscheineAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	private static void rueckgeldAusgeben(Scanner tastatur) {
		double rückgabebetrag = eingezahlterGesamtbetrag(tastatur) - bestellung.getGesamtBetrag();
		if (rückgabebetrag > 0) {
			System.out.println();
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");
			
			while(rückgabebetrag > 0.05) {
				double muenzen[] = { 2.0, 1.0, 0.5, 0.2, 0.1, 0.05 };
				for(int i = 0; i < 6; i++) {
					while(rückgabebetrag >= muenzen[i]) {
						if(rückgabebetrag > 0.5) {
							muenzeAusgeben(muenzen[i], "Euro");
						} else {
							muenzeAusgeben(muenzen[i] * 100, "Cent");
						}
						rückgabebetrag -= muenzen[i];
					}
				}
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}
	
	private static void warte(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void muenzeAusgeben(double betrag, String einheit) {
		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println(df.format(betrag) + " " + einheit);
	}
}